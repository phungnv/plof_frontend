# Operator dashboard

## Configure and prepare env

Run `npm install .` and `bower install`

## Build & development

Run `grunt build` for building and `grunt serve` for preview/development.

## Local settings

You can override the sesttings/constans i.e connection url/payment keys etc in the file `local_conf.json`, the example is in file `local_conf.json.example`

## Testing

Running `grunt test` will run the unit tests with karma.
