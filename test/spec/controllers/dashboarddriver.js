'use strict';

describe('Controller: DashboarddriverCtrl', function () {

  // load the controller's module
  beforeEach(module('alarmsystemsApp'));

  var DashboarddriverCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DashboarddriverCtrl = $controller('DashboarddriverCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DashboarddriverCtrl.awesomeThings.length).toBe(3);
  });
});
