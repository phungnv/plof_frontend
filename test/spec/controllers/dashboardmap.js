'use strict';

describe('Controller: DashboardmapCtrl', function () {

  // load the controller's module
  beforeEach(module('cleveradApp'));

  var DashboardmapCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DashboardmapCtrl = $controller('DashboardmapCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DashboardmapCtrl.awesomeThings.length).toBe(3);
  });
});
