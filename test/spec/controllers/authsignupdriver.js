'use strict';

describe('Controller: AuthsignupdriverctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('alarmsystemsApp'));

  var AuthsignupdriverctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthsignupdriverctrlCtrl = $controller('AuthsignupdriverctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AuthsignupdriverctrlCtrl.awesomeThings.length).toBe(3);
  });
});
