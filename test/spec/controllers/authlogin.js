'use strict';

describe('Controller: AuthloginctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('alarmsystemsApp'));

  var AuthloginctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthloginctrlCtrl = $controller('AuthloginctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AuthloginctrlCtrl.awesomeThings.length).toBe(3);
  });
});
