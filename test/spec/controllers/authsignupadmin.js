'use strict';

describe('Controller: AuthsignupadminctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('alarmsystemsApp'));

  var AuthsignupadminctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthsignupadminctrlCtrl = $controller('AuthsignupadminctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AuthsignupadminctrlCtrl.awesomeThings.length).toBe(3);
  });
});
