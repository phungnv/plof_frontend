'use strict';

describe('Controller: LoanDetailsCtrl', function () {

  // load the controller's module
  beforeEach(module('realatomApp'));

  var LoanDetailsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoanDetailsCtrl = $controller('LoanDetailsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LoanDetailsCtrl.awesomeThings.length).toBe(3);
  });
});
