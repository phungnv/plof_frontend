'use strict';

describe('Controller: DashboardindexCtrl', function () {

  // load the controller's module
  beforeEach(module('alarmsystemsApp'));

  var DashboardindexCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DashboardindexCtrl = $controller('DashboardindexCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DashboardindexCtrl.awesomeThings.length).toBe(3);
  });
});
