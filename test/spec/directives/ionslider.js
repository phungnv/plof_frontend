'use strict';

describe('Directive: IonSlider', function () {

  // load the directive's module
  beforeEach(module('cleveradApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-ion-slider></-ion-slider>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the IonSlider directive');
  }));
});
