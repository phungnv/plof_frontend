'use strict';

describe('Directive: inputCurrency', function () {

  // load the directive's module
  beforeEach(module('realatomApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<input-currency></input-currency>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the inputCurrency directive');
  }));
});
