'use strict';

describe('Directive: uiSelectRequired', function () {

  // load the directive's module
  beforeEach(module('cleveradApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ui-select-required></ui-select-required>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the uiSelectRequired directive');
  }));
});
