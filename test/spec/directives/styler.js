'use strict';

describe('Directive: styler', function () {

  // load the directive's module
  beforeEach(module('realatomApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<styler></styler>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the styler directive');
  }));
});
