'use strict';

angular
    .module('app.routes', ['ngRoute'])
    .config(config);

    function gettext(s) {
        return s;
    }

    function config ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                title : 'Index',
                body_styles: "",
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'main'
            })
            .when('/search', {
                title : 'Index',
                body_styles: "",
                templateUrl: 'views/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'main'
            })
            .when('/pay', {
                meta: {
                    'title': gettext('WeHelp! - La Mejor Aplicación de Seguridad Personal'),
                    'description': gettext("WeHelp! es la app de seguridad personal con la que recibirás asistencia personalizada siempre que tengas una emergencia con tan solo un botón. Descárgala ahora.")
                },
                body_styles: "animsition auth page-login layout-full page-dark",
                templateUrl: 'views/pay.html',
                controller: 'PayCtrl',
                controllerAs: 'pay'
            })
            .otherwise({
                redirectTo: '/'
            });

        $locationProvider.html5Mode(true);
    }
