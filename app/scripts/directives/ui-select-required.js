'use strict';

/**
 * @ngdoc directive
 * @name alarmsystemsApp.directive:uiSelectRequired
 * @description
 * # uiSelectRequired
 */
angular.module('app')
    .directive('uiSelectRequired', function () {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.uiSelectRequired = function(modelValue, viewValue) {
                    if (modelValue)
                        return true;
                    return false;
                };
            }
        };
    });
