'use strict';

/**
 * @ngdoc directive
 * @name realatomApp.directive:emailIsAvailable
 * @description
 * # emailIsAvailable
 */
angular.module('app')
  .directive('emailIsAvailable', function ($q, $timeout, Auth) {
      return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$asyncValidators.username = function(modelValue, viewValue) {

              if (ctrl.$isEmpty(modelValue)) {
                // consider empty model valid
                return $q.when();
              }

              var def = $q.defer();

              Auth.checkEmail(modelValue).then(
                  function (res) {
                      if (res.result) {
                          def.resolve();
                      } else {
                          def.reject();
                      }
                  }, function (res) {
                      def.reject();
                  }
              );

              return def.promise;
            };
        }
    };
});
