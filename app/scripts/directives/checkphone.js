'use strict';

/**
 * @ngdoc directive
 * @name cleveradApp.directive:ccformat
 * @description
 * # ccformat
 */
angular.module('app')
.directive('checkphone', function ($q, Pay) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$asyncValidators.username = function(modelValue, viewValue) {

                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty model valid
                    return $q.when();
                }

                var def = $q.defer();

                Pay.checkUser(modelValue).then(
                    function (res) {
                        if (res.result) {
                            def.resolve();
                        } else {
                            def.reject();
                        }
                    }, function (res) {
                        def.reject();
                    }
                );

                return def.promise;
            };
        }
    };
});
