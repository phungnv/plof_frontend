'use strict';

/**
 * @ngdoc directive
 * @name cleveradApp.directive:plyr
 * @description
 * # plyr
 */
angular.module('app')
  .directive('plyr', ['$timeout', function ($timeout) {
    return {
        template: '',
        scope: {
            data: '=',
        },
        link: function(scope, element, attrs) {
            var player;
            $timeout(function() {
                player = plyr.setup(element, { debug: false });
            }, 100);
            scope.$watch('data', function(n, o) {
                if (!angular.isDefined(player) || !angular.isDefined(n))
                    return;
                player[0].destroy();
                $timeout(function() {
                    player = plyr.setup(element, { debug: false });
                }, 100);
            });
        }
    }
  }]);
