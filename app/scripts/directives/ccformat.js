'use strict';

/**
 * @ngdoc directive
 * @name cleveradApp.directive:ccformat
 * @description
 * # ccformat
 */
angular.module('app')
.directive('ccformat', [ function ($timeout) {
    return {
        template: '',
        require: "ngModel",
        scope: false,
        link: function(scope, element, attrs, ngModel) {
            function cc_format(value) {
                if (!value)
                    return value;
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
                    var matches = v.match(/\d{4,20}/g);
                var match = matches && matches[0] || ''
                    var parts = []
                    for (var i=0, len=match.length; i<len; i+=4) {
                        parts.push(match.substring(i, i+4))
                    }
                if (parts.length) {
                    return parts.join(' ')
                } else {
                    return value
                }
            };

            function listener(event) {
                var value = element.val();
                element.val(cc_format(value));
            };


            ngModel.$parsers.push(function(viewValue) {
                var res = viewValue.replace(/ /g, '');
                return res;
            })

            ngModel.$render = function() {
                element.val(cc_format(ngModel.$viewValue));
            }

            element.bind('change keyup cut paste', listener);
            element.bind('keydown', function(event) {
                var key = event.keyCode
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40))
                    return
            });
        }
    }
}]);
