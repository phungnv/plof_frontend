'use strict';

/**
 * @ngdoc directive
 * @name cleveradApp.directive:plyr
 * @description
 * # plyr
 */
angular.module('app')
  .directive('kdatetime', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            datetimeOptions: '=',
        },
        link: function (scope, elem, attrs, ngModel) {
            scope.$watch('datetimeOptions', function () {
                render();
            }, true);
            var render = function () {
                var dp = jQuery(elem).datepicker(scope.datetimeOptions);
                dp.on('changeDate', function(e) {
                    scope.$apply(function () {
                        return ngModel.$setViewValue(e.target.value);
                    });
                });
            };
        }
    };
  });
