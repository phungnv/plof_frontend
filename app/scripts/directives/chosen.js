'use strict';

/**
 * @ngdoc directive
 * @name cleveradApp.directive:plyr
 * @description
 * # plyr
 */

angular.module('app')
  .directive('chosen', ['$timeout', function chosen($timeout) {
      var EVENTS, scope, linker, watchCollection;

      /*
       * List of events and the alias used for binding with angularJS
       */
      EVENTS = [{ onChange: 'change' },
                { onReady: 'chosen:ready' },
                { onMaxSelected: 'chosen:maxselected' },
                { onShowDropdown: 'chosen:showing_dropdown' },
                { onHideDropdown: 'chosen:hiding_dropdown' },
                { onNoResult: 'chosen:no_results' }
      ];

      /*
       * Items to be added in the scope of the directive
       */
      scope = {
          options: '=', // the options array
          ngModel: '=', // the model to bind to,,
          dataset: '=',
          ngDisabled: '='
      };

      /*
       * initialize the list of items
       * to watch to trigger the chosen:updated event
       */
      watchCollection = [];
      Object.keys(scope).forEach(function (scopeName) {
          watchCollection.push(scopeName);
      });

      /*
       * Add the list of event handler of the chosen
       * in the scope.
       */
      EVENTS.forEach(function (event) {
          var eventNameAlias = Object.keys(event)[0];
          scope[eventNameAlias] = '=';
      });

      /* Linker for the directive */
      linker = function ($scope, iElm_, iAttr, ngModel) {
          var iElm = jQuery(iElm_);
          var maxSelection = parseInt(iAttr.maxSelection, 10),
          searchThreshold = parseInt(iAttr.searchThreshold, 10);

          if (isNaN(maxSelection) || maxSelection === Infinity) {
              maxSelection = undefined;
          }

          if (isNaN(searchThreshold) || searchThreshold === Infinity) {
              searchThreshold = undefined;
          }

          var allowSingleDeselect = iElm.attr('allow-single-deselect') !== undefined ? true : false;
          var isMultiple = iElm.attr('multiple') !== undefined ? true : false;
          var noResultsText = iElm.attr('no-results-text') !== undefined ? iAttr.noResultsText : "No results found.";
          var disableSearch = iElm.attr('disable-search') !== undefined ? JSON.parse(iAttr.disableSearch) : false;
          var placeholderTextSingle = iElm.attr('placeholder-text-single') !== undefined ? iAttr.placeholderTextSingle : "Select an Option";
          var placeholderTextMultiple = iElm.attr('placeholder-text-multiple') !== undefined ? iAttr.placeholderTextMultiple : "Select Some Options";
          var displayDisabledOptions = iElm.attr('display-disabled-options') !== undefined ? JSON.parse(iAttr.displayDisabledOptions) : true;
          var displaySelectedOptions = iElm.attr('display-selected-options') !== undefined ? JSON.parse(iAttr.displaySelectedOptions) : true;

          iElm.chosen({
              max_selected_options: maxSelection,
              disable_search_threshold: searchThreshold,
              search_contains: true,
              allow_single_deselect: allowSingleDeselect,
              no_results_text: noResultsText,
              disable_search: disableSearch,
              placeholder_text_single: placeholderTextSingle,
              placeholder_text_multiple: placeholderTextMultiple,
              display_disabled_options: displayDisabledOptions,
              display_selected_options: displaySelectedOptions
          });

          iElm.on('change', function (evt, val) {
              if (isMultiple) {
                  if ('deselected' in val) {
                      $scope.ngModel.splice($scope.ngModel.indexOf(val.deselected),1);
                  } else {
                      if ($scope.ngModel !== undefined) {
                          $scope.ngModel.push(val.selected);
                      } else {
                          $scope.ngModel = [val.selected];
                      }
                  }
              } else {
                  $scope.ngModel = val.selected;
              }
              $scope.$apply();
          });

          $scope.$watch('ngModel', function () {
              $timeout(function () {
                  iElm.trigger('chosen:updated');
              }, 100);
          }, true);

          $scope.$watch('dataset', function () {
              $timeout(function () {
                  iElm.trigger('chosen:updated');
              }, 100);
          }, true);

          $scope.$watchGroup(watchCollection, function () {
              $timeout(function () {
                  iElm.trigger('chosen:updated');
              }, 100);
          });

          // assign event handlers
          EVENTS.forEach(function (event) {
              var eventNameAlias = Object.keys(event)[0];

              if (typeof $scope[eventNameAlias] === 'function') { // check if the handler is a function
                  iElm.on(event[eventNameAlias], function (event) {
                      $scope.$apply(function () {
                          $scope[eventNameAlias](event);
                      });
                  }); // listen to the event triggered by chosen
              }
          });
      };

      // return the directive
      return {
          scope: scope,
          restrict: 'A',
          require: '?ngModel',
          link: linker
      };
  }]);
