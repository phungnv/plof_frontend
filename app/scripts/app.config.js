'use strict';

//This will prevent Dropzone to instantiate on it's own unless you are using dropzone class for styling
//Dropzone.autoDiscover = false;

angular
    .module('app.config', [])
    .factory('httpInterceptor', ['$q', '$location', '$rootScope', 'localStorageService', httpInterceptor])
    .config(configs)
    .run(runs);

function httpInterceptor($q, $location, $rootScope, localStorageService){
    var responseError = function (rejection) {
        if (rejection.status === 401) {
            console.log('You are unauthorised to access the requested resource (401)');
        } else if (rejection.status === 404) {
            console.log('The requested resource could not be found (404)');
        } else if (rejection.status === 500) {
            console.log('Internal server error (500)');
            // $location.path('/');
        } else if (rejection.status === 403) {
            if ('Signature has expired.' === rejection.data.detail || 'Invalid signature.' === rejection.data.detail) {
                console.log('Signature has expired. (403)');
                $rootScope.isAuthorized = false;
                $rootScope.globals = {};
                localStorageService.remove('globals');
                $location.path('/').search({session_expired: 'true'});
            } else {
                console.log('Got unexpected 403:', rejection);
            }
        }
        return $q.reject(rejection);
    };

    return {
        responseError: responseError
    };
}

function configs($httpProvider, $resourceProvider, config) {
    //$httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('httpInterceptor');
    $resourceProvider.defaults.stripTrailingSlashes = false;
}

function update_meta($rootScope, gettextCatalog) {
    $rootScope.ngMeta = {
        'title': gettextCatalog.getString($rootScope.meta.title),
        'description': gettextCatalog.getString($rootScope.meta.description),
    }
}

function runs($rootScope, $location, $http, localStorageService, Common, config, gettextCatalog) {
    // keep user logged in after page refresh
    gettextCatalog.debug = true;
    gettextCatalog.baseLanguage = 'es';
    $rootScope.globals = localStorageService.get('globals') || {};
    $rootScope.locale = localStorageService.get('locale') || {};
    if (angular.isUndefined($rootScope.locale.lang)) {
        $rootScope.locale.lang = 'es';
    }
    gettextCatalog.setCurrentLanguage($rootScope.globals.lang);

    config.baseURLWs = config.baseURL.replace('http', 'ws');

    $rootScope.select_locale = function(locale) {
        $rootScope.locale.lang = locale;
    }

    $rootScope.rates = {
        'en': {
            'rate': 1,
            'sfx': ' USD',
            'pref': '$'
        },
        'es': {
            'rate': 20.041,
            'sfx': ' MXN',
            'pref': '$'
        }
    };

    Common.constants().then(function(res) {
        $rootScope.constants = {'SEARCH_PAGE': 6};

        angular.forEach(res.result, function(c) {
            $rootScope.constants[c.name] = c.values;
        });

        $rootScope.rates['es']['rate'] = $rootScope.constants['EXCHANGE_RATE'];
    });

    $rootScope.meta = {
        'title': "WeHelp! - La Mejor Aplicación de Seguridad Personal",
        'description': "WeHelp! es la app de seguridad personal con la que recibirás asistencia personalizada siempre que tengas una emergencia con tan solo un botón. Descárgala ahora."
    }
    update_meta($rootScope, gettextCatalog);

    $rootScope.canonical = '/';
    $rootScope.isAuthorized = false;
    $rootScope.config = config;
    $rootScope.loaded = false;
    $rootScope.go = function (path, param) {
        if (angular.isDefined(param)) {
            $location.path(path).search(param);
        } else {
            $location.path(path);
        }
    };
    $http.defaults.headers.common.Authorization = 'Basic';

    $rootScope.$watch('locale.lang', function(n, o) {
        if (angular.isDefined(n)) {
            $http.defaults.headers.common['x-language'] = n;
            gettextCatalog.setCurrentLanguage(n);
            localStorageService.set('locale', $rootScope.locale);
            update_meta($rootScope, gettextCatalog);
        }
    });

    $rootScope.$on('$routeChangeSuccess', function (event, currentRoute) {
        $rootScope.cur_route_class = currentRoute.controllerAs;
        $rootScope.body_styles = '';
        $rootScope.viewTitle = currentRoute.title;

        if ('meta' in currentRoute) {
            $rootScope.meta = currentRoute.meta;
        } else {
            $rootScope.meta = {
                'title': "WeHelp! - La Mejor Aplicación de Seguridad Personal",
                'description': "WeHelp! es la app de seguridad personal con la que recibirás asistencia personalizada siempre que tengas una emergencia con tan solo un botón. Descárgala ahora."
            }
        }

        update_meta($rootScope, gettextCatalog);

        if ('body_styles' in currentRoute) {
            $rootScope.body_styles = currentRoute.body_styles;
        }
    });

    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        var loggedIn = $rootScope.globals.currentUser;
        $rootScope.canonical = next.originalPath;
        if (next.restrictedPage && !loggedIn) {
            event.preventDefault();
            $rootScope.isAuthorized = false;
            $rootScope.globals = {};
            $http.defaults.headers.common.Authorization = 'Basic';
            localStorageService.remove('globals');
            $location.path('/login').search({logout: 'true'});
        }
    });
}

