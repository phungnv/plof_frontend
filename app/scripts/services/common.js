'use strict';

/**
 * @ngdoc service
 * @name realatomApp.common
 * @description
 * # common
 * Service in the realatomApp.
 */
angular.module('app')
    .service('Common', function ($http, $location, config) {
        function constants() {
            return $http.get(config.baseURL + '/api/constants/').then(function(response) {
                return response.data;
            });
        };

        function update_obj(from, to) {
            for (var attrname in from) {
                if (!attrname in to)
                    to[attrname] = "";
                if (typeof(from[attrname]) == 'object' && to[attrname])
                    update_obj(from[attrname],  to[attrname]);
                else
                    to[attrname] = from[attrname];
            }
        }

        function get_call_token() {
            return $http.get(config.baseURL + '/api/call/get_token/').then(function(response) {
                return response.data;
            });
        };


        function format_duration(d) {
            var eventMDuration = moment.duration(d, 'seconds');
            var eventDurationString = ""
            if (eventMDuration.days() > 0)
                eventDurationString += " " + eventMDuration.days() + " d.";
            if (eventMDuration.hours() > 0)
                eventDurationString += " " + eventMDuration.hours() + " h."
            if (eventMDuration.minutes() > 0)
                eventDurationString += " " + eventMDuration.minutes() + " min."
            eventDurationString += " " + moment.duration(eventMDuration.seconds(), 'seconds').seconds() + " sec."

            return eventDurationString.trim();
        }

        function search(q, page, pmin, pmax) {
            return $http.get(config.baseURL + '/api/items/?q='+q+'&page='+page+'&pmin='+pmin+'&pmax='+pmax).then(function(response) {
                return response.data;
            });
        };

        function get_alternatives(id) {
            return $http.get(config.baseURL + '/api/items/'+id+'/alternatives/').then(function(response) {
                return response.data;
            });
        };

        var service = {};

        service.constants = constants;
        service.update_obj = update_obj;
        service.get_call_token = get_call_token;
        service.format_duration = format_duration;
        service.search = search;
        service.get_alternatives = get_alternatives;

        return service;
    });
