'use strict';

/**
 * @ngdoc service
 * @name realatomApp.common
 * @description
 * # common
 * Service in the realatomApp.
 */
angular.module('app')
  .service('Pay', function ($http, $rootScope, $location, config) {
      function checkUser(phone) {
            return $http.post(config.baseURL + '/api/auth/check_phone/', {'phone': phone}).then(function(response) {
                return response.data;
            });
      }

      function binlist_lookup(cardBin) {
          return $http.get('https://lookup.binlist.net/' + cardBin).then(function(response) {
              return response.data;
          });
      }

      var service = {};

      service.checkUser = checkUser;
      service.binlist_lookup = binlist_lookup;

      return service;
  });
