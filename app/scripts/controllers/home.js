'use strict';

/**
 * @ngdoc function
 * @name app.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('HomeCtrl', function ($scope, $rootScope, $timeout) {
      $scope.q = "";

      $scope.EnterSearch = function(keyEvent) {
        if (keyEvent.which === 13)
          this.search();
      }

      $scope.search = function() {
          $rootScope.globals.q = $scope.q;
          $rootScope.go('/search', 'q='+$scope.q);
      };

      $scope.$on('$viewContentLoaded', function(){
          $rootScope.loaded = true;
          initCitybook();
          initparallax();
      });
  });
