'use strict';

/**
 * @ngdoc function
 * @name app.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('SearchCtrl', function ($scope, $location, $rootScope, $timeout, Common) {
      $scope.q = $rootScope.globals.q;
      $scope.page = [];
      $scope.pmin = '';
      $scope.pmax = '';
      $scope.search_loaded = false;
      $scope.results = {
          maxSize: 5,
          pages: {},
          count: '',
      };

      if (angular.isUndefined($scope.q)) {
          if ('q' in $location.search()) {
              $scope.q = $location.search()['q'];
          } else {
              $rootScope.go('/', '');
          }
      }
      $scope.q_orig = $scope.q;

      $scope.do_search = function(q, page) {
          if (page in $scope.results.pages) {
              $scope.page = $scope.results.pages[page];
              return;
          }
          $rootScope.show_loader = true;

          $scope.pmin = '';
          if ($('.irs-from').text().indexOf('$') !=  -1) {
              $scope.pmin = $('.irs-from').text().replace(' ', '').replace('$', '');
          }

          $scope.pmax = '';
          if ($('.irs-to').text().indexOf('$') !=  -1) {
              $scope.pmax = $('.irs-to').text().replace(' ', '').replace('$', '');
          }

          Common.search($scope.q, page, $scope.pmin, $scope.pmax).then(function(res) {
              $scope.results.count = res.count;
              $scope.results.pages[page] = res.results;
              $scope.page = $scope.results.pages[page];
              $scope.search_loaded = true;
              $rootScope.show_loader = false;
          }, function (res) {
              console.error(res);
              $rootScope.show_loader = false;
          });
      };

      $scope.find_deals = function(el) {
          if (!el.is_loading) {
              el.is_loading = true;
              Common.get_alternatives(el.id).then(function(res) {
                  el.alternatives = res.result;
                  el.is_loading = false;
                  el.is_cached = true;
              }, function(res) {
                  el.is_cached = true;
                  el.is_loading = false;
              });
          }
      };

      $scope.new_search = function() {
          $scope.q_orig = $scope.q;
          $rootScope.globals.q = $scope.q;
          $location.search('q', $scope.q);
          $scope.results.pages = {};
          $scope.load_page(1);
        // $rootScope.go('/search', 'q='+$scope.q);
      };

      $scope.load_page = function(page) {
          $scope.do_search($scope.q, page);
      };

      $scope.results.current = 1;

      $scope.$watch('results.current', function() {
          $scope.load_page($scope.results.current);
      });

      $scope.$on('$viewContentLoaded', function(){
          $rootScope.loaded = true;
          initCitybook();
          initparallax();
      });
  });
