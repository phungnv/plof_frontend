'use strict';

/**
 * @ngdoc function
 * @name app.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('PayCtrl', function ($rootScope, $scope, $timeout, gettextCatalog, Pay) {
      $scope.loading = false;
      $scope.forms = {};
      $scope.step = 1;
      $scope.years = [];

      var i = 0;
      var year = (new Date()).getFullYear();
      for (i; i<=10; i++) {
          $scope.years.push(year - 2000 + i);
      }

      $scope.phoneCode = "+52";
      $scope.phoneMask = "999 999 9999";
      $scope.phonePlaceholder = "xxx xxx xxxx";

      $scope.updatePhoneCode = function(country) {
          $scope.phoneCode = "+"+country.dialCode;
      }

      $scope.updatePhoneMask = function(placeholder) {
          $scope.payment.phone = "";
          $scope.phoneMask = placeholder.replace(/\d/g, '9');
          $scope.phonePlaceholder = placeholder.replace(/\d/g, 'x');
      }

      $scope.select = function(type) {
          $scope.payment.selected_period = type;
      };

      $scope.back = function() {
          if (1 == $scope.step) {
              $rootScope.go('/why-premium');
          } else {
              $scope.step = 1;
          }
      };

      $scope.reset_state = function() {
          $scope.payment = {
              selected_period: ''
          };
          $scope.new_card = {
              'provider': 'stripe'
          };
          $scope.submitted = false;
      }
      $scope.reset_state();

      $scope.next = function() {
          $scope.submitted = true;
          if (1 == $scope.step) {
              if ($scope.forms.step1.$valid) {
                  $scope.submitted = false;
                  $scope.step = 2;
              };
          } else if (2 == $scope.step) {
              if ($scope.forms.step2.$valid) {
                  $scope.loading = true;
                  Pay.binlist_lookup($scope.new_card.card_number.substring(0, 8)).then(function (response) {
                      if (response.scheme) {
                          $scope.new_card.brand = response.scheme
                      }
                      if (response.country && response.country.alpha2 == 'MX') {
                          $scope.new_card.provider = 'openpay'
                      } else {
                          $scope.new_card.provider = 'stripe'
                      }

                      if ($scope.new_card.provider == 'conekta') {
                          Conekta.token.create(
                              $('form.step2'),
                              function(token) {
                                  Pay.complete_payment(token.id, 'conekta', $scope.payment).then(
                                      function (res) {
                                          $scope.loading = false;

                                          $rootScope.go('/');

                                          swal({
                                              title: gettextCatalog.getString("Payment"),
                                              text: gettextCatalog.getString("Payment successful"),
                                              type: "success"
                                          });
                                      },
                                      function (res) {
                                          $scope.loading = false;
                                          swal({
                                              title: gettextCatalog.getString("Payment"),
                                              text: $.map(res.data, function(value, index) {return [value]; }).join(""),
                                              type: "error"
                                          });
                                      }
                                  ).catch(function () {
                                      $scope.loading = false;
                                      swal({
                                          title: gettextCatalog.getString("Payment"),
                                          text: gettextCatalog.getString("An error occurred, please try again later"),
                                          type: "error"
                                      });
                                  });
                              },
                              function(response) {
                                  $scope.loading = false;
                                  swal({
                                      title: gettextCatalog.getString("Payment"),
                                      text: response.message_to_purchaser,
                                      type: "error"
                                  });
                              }
                          );
                      } else if ($scope.new_card.provider == 'openpay') {
                          var tokenParams = {
                              "device_session_id": $rootScope.deviceSessionId,
                              "card_number": $scope.new_card.card_number,
                              "holder_name": $scope.new_card.card_name,
                              "expiration_year": $scope.new_card.card_year,
                              "expiration_month": $scope.new_card.card_month,
                              "cvv2": $scope.new_card.card_cvv,
                          };

                          OpenPay.token.create(
                              tokenParams,
                              function(token) {
                                  Pay.complete_payment(token.data.id, 'openpay', $scope.payment).then(
                                      function (res) {
                                          $scope.loading = false;

                                          $rootScope.go('/');

                                          swal({
                                              title: gettextCatalog.getString("Payment"),
                                              text: gettextCatalog.getString("Payment successful"),
                                              type: "success"
                                          });
                                      },
                                      function (res) {
                                          $scope.loading = false;
                                          swal({
                                              title: gettextCatalog.getString("Payment"),
                                              text: $.map(res.data, function(value, index) {return [value]; }).join(""),
                                              type: "error"
                                          });
                                      }
                                  ).catch(function () {
                                      $scope.loading = false;
                                      swal({
                                          title: gettextCatalog.getString("Payment"),
                                          text: gettextCatalog.getString("An error occurred, please try again later"),
                                          type: "error"
                                      });
                                  });
                              },
                              function(response) {
                                  $scope.loading = false;
                                  swal({
                                      title: gettextCatalog.getString("Payment"),
                                      text: response.data.description,
                                      type: "error"
                                  });
                              }
                          );
                      } else {
                          Stripe.card.createToken(
                              $('form.step2'),
                              function(status, response) {
                                  if (response.error) {
                                      $scope.loading = false;
                                      swal({
                                          title: gettextCatalog.getString("Payment"),
                                          text: response.error.message,
                                          type: "error"
                                      });

                                  } else {
                                      var token = response.id;
                                      Pay.complete_payment(token, 'stripe', $scope.payment).then(
                                          function (payment) {
                                              $scope.loading = false;

                                              $rootScope.go('/');

                                              swal({
                                                  title: gettextCatalog.getString("Payment"),
                                                  text: gettextCatalog.getString("Payment successful"),
                                                  type: "success"
                                              });
                                          }, function (res) {
                                              $scope.loading = false;
                                              swal({
                                                  title: gettextCatalog.getString("Payment"),
                                                  text: $.map(res.data, function(value, index) {return [value]; }).join(""),
                                                  type: "error"
                                              });
                                          }
                                      ).catch(function () {
                                          $scope.loading = false;
                                          swal({
                                              title: gettextCatalog.getString("Payment"),
                                              text: gettextCatalog.getString("An error occurred, please try again later"),
                                              type: "error"
                                          });
                                      });
                                  }
                              }
                          );
                      }
                  }).catch(function (e) {
                      $scope.loading = false;
                      swal({
                          title: gettextCatalog.getString("Payment"),
                          text: gettextCatalog.getString("An error occurred, please try again later"),
                          type: "error"
                      });
                  });
              };
          }
      };
  });
