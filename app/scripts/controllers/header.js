'use strict';

/**
 * @ngdoc function
 * @name app.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the app
 */
angular.module('app')
  .controller('HeaderCtrl', function ($scope, $timeout) {
      $timeout(function(){
          $(window).scroll(function () {
              var orgElement = $('header#top');
              var orgElementPos = $('section#content').offset();
              if (orgElementPos) {
                  var orgElementTop = orgElementPos.top;

                  if ($(window).scrollTop() >= (orgElementTop)) {
                      // scrolled past the original position; now only show the cloned, sticky element.

                      // Cloned element should always have same left position and width as original element.
                      var coordsOrgElement = orgElement.offset();
                      var leftOrgElement = coordsOrgElement.left;
                      var widthOrgElement = orgElement.css('width');

                      orgElement.addClass('cloned').css('position','fixed').css('margin-top','0').css('z-index','500').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement);
                  } else {
                      orgElement.removeClass('cloned').css('position','').css('margin-top','').css('z-index','').css('left','').css('top','').css('width','');
                  }
              }
          });
      });
  });
