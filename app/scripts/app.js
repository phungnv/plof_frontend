'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
angular
  .module('app', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngFileUpload',
    'ngTouch',
    'gettext',
    'updateMeta',
    'LocalStorageModule',
    'angularMoment',
    'ui.bootstrap',
    'app.routes',
    'app.constants',
    'app.config'
  ]);
