(function() {
    'use strict';
    angular
        .module('app')
        .filter('length', length);

    /** @ngInject */
    function length() {
        return function(items) {
            var res = '';

            if (!items) {
                return 0;
            }

            if (items.constructor == Array){
                res = items.length;
            }
            else if (items.constructor == Object){
                res = Object.keys(items).length;
            }

            return res;
        };
    }
})();
