'use strict';

/**
 * @ngdoc filter
 * @name kimikFrontendApp.filter:currency
 * @function
 * @description
 * # currency
 * Filter in the app.
 */
angular.module('app')
  .filter('currencyconv_sfx', function ($rootScope, $filter) {
    return function (input, locale) {
        var sfx = '';
        if (locale in $rootScope.rates) {
            sfx = $rootScope.rates[locale]['sfx'];
        }
        return sfx;
    };
  })
  .filter('currencyconv', function ($rootScope, $filter) {
    return function (input, locale) {
        var rate = 1;
        var sfx = '';
        var pref = '';
        if (locale in $rootScope.rates) {
            rate = $rootScope.rates[locale]['rate'];
            sfx = $rootScope.rates[locale]['sfx'];
            pref = $rootScope.rates[locale]['pref'];
        }
        return pref + $filter('number')(parseFloat(input) * rate, 1) + sfx;
    };
  })
  .filter('currencyconv_html', function ($rootScope, $filter, $sce) {
    return function (input, locale) {
        var rate = 1;
        var sfx = '';
        var pref = '';
        if (locale in $rootScope.rates) {
            rate = $rootScope.rates[locale]['rate'];
            sfx = $rootScope.rates[locale]['sfx'];
            pref = $rootScope.rates[locale]['pref'];
        }
        return $sce.trustAsHtml(pref
            + $filter('number')(parseFloat(input) * rate, 1)
            + "<b class=\"currency_sfx\">" + sfx + "</b>");
    };
  })
  .filter('currencyconv_pay', function ($rootScope, $filter, $sce) {
    return function (input, locale) {
        var rate = 1;
        var sfx = '';
        var pref = '';
        if (locale in $rootScope.rates) {
            rate = $rootScope.rates[locale]['rate'];
            sfx = $rootScope.rates[locale]['sfx'];
            pref = $rootScope.rates[locale]['pref'];
        }
        var price = Math.round(parseFloat(input) * rate);

        return $sce.trustAsHtml(
            "<b>" + pref + price + "</b>"
            + "<span>" + sfx + "</span>"
        );
    };
  });
