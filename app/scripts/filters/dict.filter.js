'use strict';

angular.module('app')
    .filter('inArray', function($filter){
        return function(list, arrayFilter, element){
            if(arrayFilter){
                return $filter("filter")(list, function(listItem){
                    return arrayFilter.indexOf(listItem[element]) != -1;
                });
            }
        };
    })
    .filter('toArray', function() { return function(obj) {
        if (!(obj instanceof Object)) return obj;
        return jQuery.map(obj, function(val, key) {
            return Object.defineProperty(val, '$key', {__proto__: null, value: key});
        });
    }})
    .filter('dictfilter', function() {
        return function(objects, filter) {
            if (!objects) return {};
            if (!filter) return objects;

            var filteredObject = {};

            objects.forEach(function(object) {
                var match = 0;
                Object.keys(filter).forEach(function(key) {
                    if (object[key][field] === filter) {
                        filteredObject[key] = object[key];
                    }
                });
            });

            return filteredObject;

        }
    });
